﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Task01
{
    public partial class MainPage : ContentPage
    {
        static string result = "The sum is: ";
        public MainPage()
        {
            InitializeComponent();
        }

        public void buttonSubmitclick(object sender, EventArgs e)
        {
            var input = buttonClicked.click1(entry1.Text);
            Label3.Text = result + input;
        }
    }
}
