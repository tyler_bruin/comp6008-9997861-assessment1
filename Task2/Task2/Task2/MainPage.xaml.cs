﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Task2
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }

        static int score = 0;
        static int attempts = 0;
        static int random = 0;

        public void buttonSubmitclick(object sender, EventArgs e)
        {
            Background.BackgroundColor = Color.FromHex($"#FFF");

            int guess = 0;
            guess = int.Parse(entry1.Text);

            var a = new function();

            var ran = a.randomNumber(random);

            if (a.randomNumber(random) == guess)
            {
                score = score + 1;
                Background.BackgroundColor = Color.FromHex($"#47d147");
               // entry1.Text = ""; // Remove this line for easy testing
            }
            else
            {
                
                Background.BackgroundColor = Color.FromHex($"#ff3333");
            }

            attempts++;
            Label2.Text = ("Number of games won: " + score);
            Label4.Text = ("Number of attempts: " + attempts);
            Label3.Text = ("Correct Guess: " + ran);
        }

    }
}